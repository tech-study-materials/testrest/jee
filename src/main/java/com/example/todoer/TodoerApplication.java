package com.example.todoer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.stream.Collectors.toUnmodifiableList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@ApplicationPath("/")
@Path("/")
public class TodoerApplication extends Application implements TodoerApi {

    @Override
    public Set<Class<?>> getClasses() {
        return Set.of(TodoerApplication.class);
    }

    Repo repo = InMemRepo.INSTANCE;

    @Override
    @POST
    @Consumes(APPLICATION_JSON) @Produces(APPLICATION_JSON)
    @Path("/{user}/todo")
    public IdResponse createTodo(@PathParam("user") String user, NewTodoRequest req) {
        return new IdResponse(repo.create(user, req.getDescription()));
    }

    @Override
    @GET
    @Consumes(APPLICATION_JSON) @Produces(APPLICATION_JSON)
    @Path("/{user}/todo")
    public List<Todo> getTodos(@PathParam("user") String user) {
        return repo.findByUser(user);
    }

    @Override
    @DELETE @Consumes(APPLICATION_JSON) @Produces(APPLICATION_JSON)
    @Path("/{user}/todo/{id}")
    public void markDone(@PathParam("user") String user, @PathParam("id") long id) {
        repo.deleteById(id);
    }
}

//let's define REST API:
interface TodoerApi {

    @POST
    @Consumes(APPLICATION_JSON) @Produces(APPLICATION_JSON)
    @Path("/{user}/todo")
    IdResponse createTodo(@PathParam("user") String user, NewTodoRequest req);

    @GET
    @Consumes(APPLICATION_JSON) @Produces(APPLICATION_JSON)
    @Path("/{user}/todo")
    List<Todo> getTodos(@PathParam("user") String user);

    @DELETE @Consumes(APPLICATION_JSON) @Produces(APPLICATION_JSON)
    @Path("/{user}/todo/{id}")
    void markDone(@PathParam("user") String user, @PathParam("id") long id);
}

//Let's define models:
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Todo {
    long id;
    String user;
    String description;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class NewTodoRequest {
    String description;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class IdResponse {
    long id;
}

//let's define persistence
interface Repo {
    long create(String user, String description);
    List<Todo> findByUser(String user);
    void deleteById(long id);
    void deleteAll();
}

class InMemRepo implements Repo {
    static final Repo INSTANCE = new InMemRepo();

    final AtomicLong sequence = new AtomicLong();
    List<Todo> todos = new ArrayList<>();

    @Override
    public long create(String user, String description) {
        long id = sequence.incrementAndGet();
        todos.add(new Todo(id, user, description));
        return id;
    }

    @Override
    public List<Todo> findByUser(String user) {
        return todos.stream()
                .filter(t -> user.equals(t.user))
                .collect(toUnmodifiableList());
    }

    @Override
    public void deleteById(long id) {
        todos.stream()
                .filter(t -> id == t.id)
                .findFirst()
                .ifPresent(t -> todos.remove(t));
    }

    @Override
    public void deleteAll() {
        todos.clear();
    }
}