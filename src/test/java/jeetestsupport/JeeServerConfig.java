package jeetestsupport;

import lombok.RequiredArgsConstructor;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.function.Supplier;

@RequiredArgsConstructor
public class JeeServerConfig implements BeforeAllCallback, AfterAllCallback {

    final Supplier<Application> applicationToDeploy;
    JeeServer server;

    @Override
    public void beforeAll(ExtensionContext extensionContext) throws Exception {
        server = JeeServer.serve(applicationToDeploy.get());
    }

    @Override
    public void afterAll(ExtensionContext extensionContext) throws Exception {
        server.stop();
    }

    public int port() {
        return server.port();
    }

    interface JeeServer {

        void stop();
        int port();

        static JeeServer serve(Application application) {
            URI uri = UriBuilder.fromUri("http://localhost").port(0).build();
            ResourceConfig cfg = ResourceConfig.forApplication(application);
            var server = GrizzlyHttpServerFactory.createHttpServer(uri, cfg, true);
            int port = server.getListeners().stream().findFirst().get().getPort();
            return new JeeServer() {
                @Override
                public void stop() {
                    server.shutdownNow();
                }

                @Override
                public int port() {
                    return port;
                }
            };
        }
    }
}
