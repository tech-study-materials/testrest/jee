package com.example.todoer;

import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.jaxrs.JAXRSContract;
import feign.okhttp.OkHttpClient;
import jeetestsupport.JeeServerConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import static org.assertj.core.api.Assertions.assertThat;

public class FeignClientTests {

    @RegisterExtension static JeeServerConfig jeeServer = new JeeServerConfig(TodoerApplication::new);

    static TodoerApi api;

    @BeforeAll
    static void createApi() {
        api = Feign.builder()
                .client(new OkHttpClient())
                .contract(new JAXRSContract())
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(TodoerApi.class, "http://localhost:" + jeeServer.port());
    }


    @BeforeEach
    void cleanDb() {
        InMemRepo.INSTANCE.deleteAll(); //repo should be injected, pardon my ignorance of CDI
    }


    @Test
    void testCreateAndGet() {
        //given
        api.createTodo("adam", new NewTodoRequest("buy milk"));

        //when
        var todos = api.getTodos("adam");

        //then
        assertThat(todos)
                .usingElementComparatorIgnoringFields("id")
                .containsExactlyInAnyOrder(
                        Todo.builder().user("adam").description("buy milk").build()
                );
    }

    @Test
    void testMarkDone() {
        //given
        long buyMilkId = api
                .createTodo("adam", new NewTodoRequest("buy milk"))
                .getId();

        //when
        api.markDone("adam", buyMilkId);

        //then
        var todos = api.getTodos("adam");
        assertThat(todos).isEmpty();
    }
}

