package com.example.todoer;

import jeetestsupport.JeeServerConfig;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.net.URI;
import java.net.URISyntaxException;

import static org.assertj.core.api.Assertions.assertThat;

public class MicroProfileClientTests {

    @RegisterExtension static JeeServerConfig jeeServer = new JeeServerConfig(TodoerApplication::new);

    static TodoerApi api;

    @BeforeAll
    static void createApi() throws URISyntaxException {
        api = RestClientBuilder.newBuilder()
                .baseUri(new URI("http://localhost:" + jeeServer.port()))
                .build(TodoerApi.class);
        //or: @RegisterRestClient - but I didn't figure out how to use it. Extend JerseyTest?
    }

    @BeforeEach
    void init() {
        InMemRepo.INSTANCE.deleteAll(); //repo should be injected, pardon my ignorance of CDI
    }

    @Test
    void testCreateAndGet() {
        //given
        api.createTodo("adam", new NewTodoRequest("buy milk"));

        //when
        var todos = api.getTodos("adam");

        //then
        assertThat(todos)
                .usingElementComparatorIgnoringFields("id")
                .containsExactlyInAnyOrder(
                        Todo.builder().user("adam").description("buy milk").build()
                );
    }

    @Test
    void testMarkDone() {
        //given
        long buyMilkId = api
                .createTodo("adam", new NewTodoRequest("buy milk"))
                .getId();

        //when
        api.markDone("adam", buyMilkId);

        //then
        var todos = api.getTodos("adam");
        assertThat(todos).isEmpty();
    }
}